This repository is part of the 'watchdog CI' project described in this [RFC](https://blueprints.ebury.rocks/components/continuous_integration/watchdog/)

Its objective is to provide a tool for analyzing the results of the jenkins tests to help in the identification of 
flaky errors for later action.