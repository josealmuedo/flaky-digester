# flaky-digester

## Table of contents

1. [Vision](VISION.md): What is the meaning of this service?
3. [Contributing](CONTRIBUTING.md): How to contribute to the project?
4. [Code Owners](CODEOWNERS.md): Who are the maintainers? 
4. [Installation](#installation): Instructions to install the script.
5. [Use](#use): Instructions to use the script.

## Installation

-

### Clone the repository

-

### Running the application

-

## Use

-