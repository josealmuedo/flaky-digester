# Contributing

Thank you for reading this document about how to contribute to this project. If you are here it is because you are thinking to contribute to this great service. In the following sections, we explain you how to proceed in the different cases and what are the quality guides.

## How to submit changes

These are the steps to contribute with a new change to this repository:

1. Create a **ticket** in JIRA in your project board, complete the required information, add the ticket to your current sprint and set the status to the equivalent to "in progress".
2. Create a **branch** in the repository from `master` branch. The name of the branch is the same than the JIRA ticket identifier. For example, for ticket ATA-123, the branch is named ATA-123.
3. In every **commit** messages, include a prefix [ATA-123] and then a descriptive message. Try to commit frequently and doing commits by atomic changes.
4. When your change is ready to be reviewed, create a **pull request** (PR) from the JIRA ticket (See [Pull requests](#Pull-requests) section) and move your ticket to the correct status.
5. Manage the **UAT** according to the guidelines (See [User acceptance testing](#User-acceptance-testing) section) and update the JIRA ticket status.
6. During the **code review ** or **UAT** phases, reviewers could propose changes; and it means possible changes and new commits (See [Code review](#Code-review) section).
7. When the merge conditions are fulfilled (See [Merge conditions](#Merge-conditions)) , you can **merge** the code from the PR following the **squash** strategy (see more [here](https://fxsolutions.atlassian.net/wiki/spaces/PRODUCTS/pages/952729808/Merge+strategy)) and **close the ticket** in JIRA.

### Pull requests

A **pull request** (PR) is the method of submitting contributions to the project. The project uses Bitbucket for  PR management and can be created from JIRA tickets. The PR is a process for peer review by code maintainers and relevant developers involved in the changes.

Considerations during the pull request creation:

* The destination **branch** of the pull request must be `master` branch.
* The **title** follows the format: [ticket-id] Title of the ticket. Example: [ATA-123] Changing the documentation
* Update the **description** to include a descriptive text for changes and include notes for the reviewers.
* The default **reviewers** must be the code owners defined in the [CODEOWNERS](/CODEOWNERS) file and you must  include also some code owners related with third party services, when these changes affects them.
* Mark the option "close branch after the pull request is merged".

### Code review

**Code Review** is an integral process of software development that helps identify bugs and defects before the testing **phase**. We use the pull request (PR) mechanism in Bitbucket to do this process. 

These are the considerations for the code review phase:

* The PR is considered **approved** when **some code owner** has approved the PR
* The comments can be done at PR level or at detailed level.
* The comments can be in different levels of relevance: **mandatory** or **optional**.
* The mandatory changes must have a associated **task** in the pull request view.
* All the mandatory changes must be changed and the associated tasks marked as **done**.
* The code owners review the manual checks for **quality gates** of the project. (See [Quality gates](#Quality-gates) section)
* If the change is rejected by the code owners and there is not possibility of modifications, the PR will be marked as **declined**, and the process do not continue. If it can be modified, the pull request should not be declined, only commented with mandatory changes.
* The code review phase is considered **finished** when all the mandatory changes are done and it is approved.

### Merge conditions

Before merging a code in a pull request to the master branch, it is required to comply with **all** the next conditions:

*  The **code review** phase has **finished**.
* The **CI** (continuous integration) process marks the PR as **green**.
* There is **not merge conflicts**.
* The branch must be **updated** with the latest commit of `master` branch.
* The **ticket** information is **completed** (see required fields [here](https://fxsolutions.atlassian.net/wiki/spaces/PRODUCTS/pages/913015365/Merging+code)).

## How to report a bug

Depending on the kind of reporter, this is the way you can notify a new bug:

* If you are a **contributor or code owner** and detect a bug in this project, you must create a JIRA ticket in your board with task type **defect** (see [here](https://fxsolutions.atlassian.net/wiki/spaces/PRODUCTS/pages/123095173/Bugs+and+e2e+philosophy)).  Where:
  * If it is detected during the Software Development Life Cycle [(SDLC)](https://fxsolutions.atlassian.net/wiki/spaces/PRODUCTS/pages/42795535/SDLC), you must create it as subtask of the main task.
  * In other cases, as task.

If the code owners or some contributors try to solve the bug, they will move the ticket to their JIRA board.

## How to request an enhancement

Contact with some AUT member. Thank you for your contributions!

## Style guide

This project has the following conventions:

* Document your code (See [docstring rules](https://fxsolutions.atlassian.net/wiki/spaces/PRODUCTS/pages/83886143/Documentation))
* Tests use [pytest](https://docs.pytest.org/en/latest/). Mark integration tests with *pytest.mark.integration* and unit tests with *pytest.mark.unit*.
* Exceptions are better than error codes.
* Create a new file for every new model.